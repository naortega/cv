# Curriculum Vitæ

This PDF contains my current curriculum. It's written in English and Spanish.
The document itself is written with LaTeX, and compiled with XeLaTeX.

The template itself is not mine, look at the headers of the files for copyright
information.
